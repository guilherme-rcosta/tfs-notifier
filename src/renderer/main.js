import Vue from 'vue';
import axios from 'axios';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faThumbtack, faCog, faArrowCircleLeft, faHome, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import Config from 'electron-config';
import AutoLaunch from 'auto-launch';

import App from './App';
import router from './router';
import store from './store';

if (!process.env.IS_WEB) Vue.use(require('vue-electron'));

const settings = new Config({
  defaults: {
    tfs: 'http://bahrein:8080/tfs',
    organization: 'DPT',
    projects: ['SDP_SVC', 'SDP_PTC'],
    token: '2gmefjobwtn7ywjkxqvzh6ej67ailyp256aomvlkqrqm3lzfc2kq',
    refreshInterval: 5, // seconds
  },
});

const autoLauncher = new AutoLaunch({ name: 'tfs-notifier' });

axios.defaults.baseURL = `${settings.get('tfs')}/${settings.get('organization')}`;

Vue.settings = Vue.prototype.$settings = settings;
Vue.http = Vue.prototype.$http = axios;
Vue.config.productionTip = false;
Vue.autoLauncher = Vue.prototype.$autoLauncher = autoLauncher;

library.add([faThumbtack, faCog, faArrowCircleLeft, faHome, faInfoCircle]);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(BootstrapVue);

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>',
}).$mount('#app');
